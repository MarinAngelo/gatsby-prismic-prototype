module.exports = {
  siteMetadata: {
    title: `Gatsby Prismic Prototype`,
    description: `A prototype website for klap-web`,
    author: `Angelo Klap`,
  },
  plugins: [`gatsby-plugin-fontawesome-css`,
    `gatsby-source-fontawesome`,
    {
      resolve: "gatsby-plugin-anchor-links",
      options: {
        offset: -100
      }
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [`Megrim`, `Sniglet`, `Raleway`],
      },
    },
    "gatsby-plugin-styled-components",
    {
      resolve: "@prismicio/gatsby-source-prismic-graphql",
      options: {
        repositoryName: "kwdmaster",
        path: '/preview',
        previews: true,
        pages: [
          {
            type: "Page",
            match: "/:uid",
            path: "/page-preview",
            component: require.resolve("./src/templates/page.js"),
          },
        ],
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
