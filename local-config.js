const locals = {
  prismicPreviewScript: `<script async defer src="https://static.cdn.prismic.io/prismic.js?new=true&repo=kwdmaster"></script>`
}

export { locals }