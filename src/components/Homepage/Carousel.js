import React from 'react'
import Carousel from 'react-bootstrap/Carousel'
import styled from 'styled-components'
import RichText from "../richText"

const StyledCarousel = styled(Carousel)`
  margin-left: -15px;
  margin-right: -15px;
`

export default function Carousell({ images }) {
  return (
    <StyledCarousel fade>
      {images.fields.map((image, i) => (
        <Carousel.Item key={i}>
          <a href={image.link}>
            <img
              className="d-block w-100"
              src={image.image.url}
              alt={image.alt}
            />
          </a>
          <Carousel.Caption>
            <RichText render={image.caption_title} />
            <RichText render={image.caption_text} />
          </Carousel.Caption>
        </Carousel.Item>
      ))}
    </StyledCarousel>
  )
}
