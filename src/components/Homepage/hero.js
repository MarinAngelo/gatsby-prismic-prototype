import React from "react"
import RichText from "../richText"
import styled from "styled-components"
import { hexToRgbA, darkenLighten } from '../common/ColorAdjust'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Container from 'react-bootstrap/Container'
import StyledLinkButton from '../common/StyledLinkButton'

const StyledJumbotron = styled(Jumbotron)`
// Extra small devices (portrait phones, less than 576px)
// No media query for xs since this is the default in Bootstrap

    background-image:
     /* linear-gradient(to bottom, rgba(245, 246, 252, 0.52), rgba(117, 19, 93, 0.73)),    */
     url(${props => props.bgimagephone}) !important;

    margin-left: -15px;
    margin-right: -15px;
    background-color: ${props => props.bgcolor};
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    height: 100vh;
    /* make background-color shine throug the image */
    /* opacity: 0.99; */
    /* make the bgimage darker */

    .jumbo-text-box {
        background-color: ${props => props.phone_banner_bg_color};
        padding: 2rem;
        height: 100vh;
        margin-top: -25px;
        text-align: center;
        /* Mutes everything also text, as implemented now only bg is transparent and not the text */
        /* opacity: ${props => props.banner_bg_transparency} !important; */

        h1, h2 {
            color: var(--header-link-color) !important;
            /* color: ${props => props.text_color} !important; */
        }

        h1{
            margin-top: 15vh;
            /* To avoid collapsing margins, add the margin-bottom: 0; rule */
            margin-bottom: 0;
        }
        
        h2{
            font-size: 1.8rem;
            margin-top: 6rem;
            margin-bottom: 6rem;
        }
    }

// Small devices (landscape phones, 576px and up, col-sm)
@media (min-width: 576px) {
    background-image: url(${props => props.bgimagephonelandscape}) !important;

    .jumbo-text-box {
        max-width: 100%;
        margin-top: -55px;

        h1{
            margin-top: 14vh;
            font-size: 1.8rem;
        }
        
        h2{
            margin-top: 1.8rem;
            margin-bottom: 2.4rem;
            font-size: 1.4rem;
        }
    }
}
// Medium devices (tablets, 768px and up, col-md)
@media (min-width: 768px) {
    background-image: url(${props => props.bgimagetablet}) !important;
    .jumbo-text-box {
        margin-top: -65px;
        padding: 4rem !important;
        
        h1{
            margin-top: 30vh;
            font-size: 2.2rem;
        }

        h2{
            margin-top: 5rem;
            margin-bottom: 6rem;
            font-size: 1.6rem;
        }
    }
    
}
// Large devices (desktops, 992px and up, col-lg)
@media (min-width: 992px) {
    background-image:
     /* linear-gradient(to bottom, rgba(245, 246, 252, 0.52), rgba(117, 19, 93, 0.73)),    */
     url(${props => props.bgimage}) !important;
    filter: brightness(75%);

    .jumbo-text-box {
        background-color: ${props => props.banner_bg_color};
        max-width: 45%;
        height: auto;
        margin-left: 5vw;
        margin-top: 15vh;
        padding: 2rem;
        border-radius: 50px;
        text-align: center;

        h1, h2 {
            color: ${props => props.text_color} !important;
        }

        h1{ 
            margin-top: 0;
            /* To avoid collapsing margins, add the margin-bottom: 0; rule */
            margin-bottom: 0;
            color: ${props => props.text_color} !important;
        }
        
        h2{
            font-size: 1.8rem;
            margin-top: 1.5rem;
            margin-bottom: 2rem;
        }
    }
 
}
// Extra large devices (large desktops, 1200px and up, col-xl)
@media (min-width: 1200px) {
 
}
    
`;

const StyledButton = styled(StyledLinkButton)`

// Extra small devices (portrait phones, less than 576px)
// No media query for xs since this is the default in Bootstrap
background: ${props => props.button_transparent ? "transparent" : props.button_bg_color} !important;
font-size: 1.5rem;
padding: 0.8rem 1.6rem;
border-radius: 50px;
color: var(--header-link-color) !important;
border-color: var(--header-link-color);

    // same as color to avoid changeing
    &:visited{
        color: var(--header-link-color) !important;
    }

// Small devices (landscape phones, 576px and up, col-sm)
@media (min-width: 576px) {
    font-size: 1rem;
    padding: 0.6rem 1.4rem;
 
}
// Medium devices (tablets, 768px and up, col-md)
@media (min-width: 768px) {
    font-size: 1.5rem;
    padding: 0.8rem 1.6rem;
 
}
// Large devices (desktops, 992px and up, col-lg)
@media (min-width: 992px) {
  color: ${props => props.button_color} !important;
  border-color: ${props => props.button_color};

    &:hover, &:active{
    color: ${props => darkenLighten(props.button_color, 190)} !important;
    border-color: ${props => props.button_color ? darkenLighten(props.button_color, 60) : null};
    background: ${props => props.button_bg_color} !important;
    }

    &:visited{
    color: ${props => props.button_color} !important;
    border-color: ${props => props.button_color};

    }
 
}
// Extra large devices (large desktops, 1200px and up, col-xl)
@media (min-width: 1200px) {
 
}
`;

const Hero = ({
    title,
    content,
    ctabuttonlabel,
    ctabuttontarget,
    transparentnavbar,
    bgimage,
    bgimagetablet,
    bgimagelaptop,
    bgimagephone,
    bgimagephonelandscape,
    bgcolor,
    text_color,
    banner_bg_color,
    banner_bg_transparency,
    button_bg_color,
    button_color,
    button_transparent
}) => {

    const defaultColors = {
        bgcolor: "#000000", // black
        text_color: "#5A5AC6", // dark violoet
        banner_bg_color: "#7999d8", // blue
        banner_bg_transparency: 50,
        button_bg_color: "#7373ef", // light violet
        button_color: "#3434bb", // violet
        button_transparent: 1
    }

    if(button_transparent === null) {
        button_transparent = defaultColors.button_transparent;
    }

    if(bgcolor === null){
        bgcolor = defaultColors.bgcolor;
    }

    if(banner_bg_transparency === null){
        banner_bg_transparency = defaultColors.banner_bg_transparency;
    }

    if(banner_bg_color === null){
        banner_bg_color = defaultColors.banner_bg_color;

    }
    
    // darken the bg color
    let phone_banner_bg_color = darkenLighten(banner_bg_color.toString(), -120);

    if (banner_bg_transparency) {
            banner_bg_color = hexToRgbA(banner_bg_color, (banner_bg_transparency / 100));
            phone_banner_bg_color = hexToRgbA(phone_banner_bg_color, (banner_bg_transparency / 100));
        }

    if (button_bg_color === null) {
        button_bg_color = defaultColors.button_bg_color;
    }

    if (button_color === null) {
        button_color = defaultColors.button_color;
    }

    if (text_color === null) {
        text_color = defaultColors.text_color;
    }

    
    return (
        <StyledJumbotron fluid
            navbarstyle={transparentnavbar}
            bgimage={bgimage}
            bgimagetablet={bgimagetablet}
            bgimagelaptop={bgimagelaptop}
            bgimagephone={bgimagephone}
            bgimagephonelandscape={bgimagephonelandscape}
            bgcolor={bgcolor}
            text_color={text_color}
            banner_bg_color={banner_bg_color}
            banner_bg_transparency={banner_bg_transparency}
            phone_banner_bg_color={phone_banner_bg_color}
            // Solution if ocapacy is used instead of bg transp with rgba
            // banner_bg_transparency={(banner_bg_transparency / 100)}
        >
            <Container className="jumbo-text-box">
                <RichText render={title} />
                <RichText render={content} />
                <StyledButton
                    to={`${ctabuttontarget}`}
                    button_bg_color={button_bg_color}
                    button_color={button_color}
                    button_transparent={button_transparent}
                >
                {ctabuttonlabel}
                </StyledButton>
            </Container>
        </StyledJumbotron>
    )
}

export default Hero;