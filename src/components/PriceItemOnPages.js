import React from 'react'
import RichText from "./richText"
import styled from "styled-components"
import { Card } from "react-bootstrap"

const StyledCard = styled(Card)`
    // determins breakepoint
    min-width: 24rem;
    margin-bottom: 1.5rem !important;
    background-color: ${p => p.annex === "Am beliebtesten" ? 'orange' : 'var(--component-bg-color)'};

    .annex{
        background-color: ${p => p.annex === "Am beliebtesten" ? 'var(--component-bg-color)' : 'orange'};
        position: absolute;
        right: 0;
        top: 0;
        padding: 10px 15px;
        color: var(--page-color);
        font-weight: bold;
    }
`;

export default function PriceItemOnPages({ title, price, description, annex, type }) {
    return (
        <StyledCard annex={annex}>
            <Card.Body>
                {!!annex &&
                    <div className="annex">
                        {annex}
                    </div>
                }
                <RichText render={title} />
                <div className="price">
                    <p>
                        CHF {new Intl.NumberFormat('de-CH').format(price)}.00 <span className="duration">{type}</span>
                    </p>
                    {/* CHF {price} <span className="duration"></span> */}
                </div>
                <div className="description">
                    <RichText render={description} />
                </div>
            </Card.Body>
        </StyledCard>
    )
}