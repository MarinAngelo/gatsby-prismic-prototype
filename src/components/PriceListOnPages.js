import React from 'react'
import PriceItem from "./PriceItemOnPages"
import { CardDeck } from 'react-bootstrap'
import SectionTitle from '../templates/SectionTitle'

export default function PriceListOnPages({ title, prices }) {
    console.log('Prices: ', prices);
    return (
        <div>
            <SectionTitle title={title} />
            <CardDeck>
                {prices.map((price, i) => {
                    return (
                        <PriceItem
                            annex={price.price_annex}
                            price={price.price}
                            title={price.price_list_title}
                            description={price.price_description}
                            type={price.price_type}
                            key={i} />
                    )
                })}
            </CardDeck>
        </div>
    )
}
