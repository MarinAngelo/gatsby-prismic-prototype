import styled from 'styled-components'
import Navbar from 'react-bootstrap/Navbar'


const StyledNavbarBrand = styled(Navbar.Brand)`
// Extra small devices (portrait phones, less than 576px)
// No media query for xs since this is the default in Bootstrap
font-family: var(--logo-font);

a.navbar-brand{
  color: var(--header-link-color) !important;
  font-weight: bold !important;
  font-size: 1.8rem;
  margin-left: 1rem;
  letter-spacing: .2rem;
}

// Small devices (landscape phones, 576px and up, col-sm)
@media (min-width: 576px) {
  
}
// Medium devices (tablets, 768px and up, col-md)
@media (min-width: 768px) {
  
}
// Large devices (desktops, 992px and up, col-lg)
@media (min-width: 992px) {
  // Efects not the logo
  position: ${props => props.pathname !== "/" ? null : props.scrolled ? null : "absolute"};
  margin-top: ${props => props.pathname !== "/" ? null : props.scrolled ? null : "-1rem"};
  font-size: ${props => props.pathname !== "/" ? "1.6rem" : props.scrolled ? "1.6rem" : "3rem"};
  margin-left: ${props => props.pathname !== "/" ? null : props.scrolled ? null : "2rem"};

  img.logo{ 
      padding: 1rem;
      margin: 1rem;
      position: absolute;
      top: 10px; left: 20px;
  }
  
}
// Extra large devices (large desktops, 1200px and up, col-xl)
@media (min-width: 1200px) {
  
}
  }`

export default StyledNavbarBrand;