import styled from 'styled-components'
import Navbar from 'react-bootstrap/Navbar'
import { hexToRgbA, darkenLighten } from './ColorAdjust'


const StyledNavbar = styled(Navbar)`
// Extra small devices (portrait phones, less than 576px)
// No media query for xs since this is the default in Bootstrap

  // Aplies also to the dropdownmenue
   background-color: ${props => props.pathname !== "/" ?
        "var(--header-bg-color)" : props.expanded ?
        hexToRgbA(props.headerbgcolor, "0.95") : props.transparentnavbar ?
        hexToRgbA(props.headerbgcolor, "0.10") : "var(--header-bg-color)"};
  // End of aplies also to the dropdownmenue

  color: var(--header-link-color);
  text-transform: uppercase;
  text-align: center;

  .nav-link, #basic-nav-dropdown{
  font-size: 1.2rem;
  }

  li.dropdown-item a{
    font-size: 1rem;
    text-transform: none;
  }
  
  .dropdown-item, .nav-link, #basic-nav-dropdown{
    color: var(--header-link-color);
  }

  .dropdown-menu{
    background-color: ${props => darkenLighten(props.headerbgcolor, 20)};
    text-align: center;
    font-size: 1.3rem;
  }

// Small devices (landscape phones, 576px and up, col-sm)
@media (min-width: 576px) {
}

// Medium devices (tablets, 768px and up, col-md)
@media (min-width: 768px) {

}

// Large devices (desktops, 992px and up, col-lg)
@media (min-width: 992px) {

    .main-nav{
        margin-right: 2rem;

    }

    // blured background behind nav only on homepage when not scrolled
    .main-nav{
        background-color: ${props => props.pathname !== "/" ? null : props.scrolled ? null : hexToRgbA(props.headerbgcolor, "0.45")};
        padding: ${props => props.pathname !== "/" ? null : props.scrolled ? null : "0.5rem 2rem"};
        margin-top: ${props => props.pathname !== "/" ? null : props.scrolled ? null : "2rem"};
        border-radius: 50px;
    }
    // End blured background behind nav

    .nav-link, #basic-nav-dropdown{
      // Should be imported from GlobalStyles
    color: ${darkenLighten("#2b5f2d", 180)} !important;
    }
    

  margin-top: 30px;

  // show transparent navbar only on the homepage
  background-color: ${props => props.pathname !== "/" ?
        "var(--header-bg-color)" : props.transparentnavbar ?
            hexToRgbA(props.headerbgcolor, (props.transparentnavbar / 100)) : hexToRgbA(props.headerbgcolor, "0.85")};

  height: ${props => props.scrolled ? "55px" : null};
  font-size: ${props => props.scrolled ? "1rem" : "1.3rem"};

    .dropdown-menu{
    background-color: ${props => hexToRgbA(props.headerbgcolor, "0.75")};
    margin-top: ${props => props.scrolled ? null : "0.8rem"} !important;
    text-align: left;
    font-size: ${props => props.scrolled ? "0.9rem" : "1.2rem"};
    line-height: 0.5;
  }

    .dropdown-item{
      &:hover{
        background-color: ${props => hexToRgbA(props.headerbgcolor, "0.95")};
      }
  }
  
}

// Extra large devices (large desktops, 1200px and up, col-xl)
@media (min-width: 1200px) {
  
}
`;

const StyledNavbarToggle = styled(Navbar.Toggle)`
  background-color: var(--header-link-color) !important;
  border: 0;
  &:focus{
    outline: none;
  }
`;

export {StyledNavbar, StyledNavbarToggle};