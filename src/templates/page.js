import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/Layout"
import PageTitle from '../components/PageTitle'
import OneColTextSection from '../templates/OneColTextSection'
import OneColSection from '../templates/OneColSection'
import PriceList from '../components/PriceListOnPages'

export const query = graphql`
  query PageQuery($id: String) {
    prismic {
           allPages(id: $id) {
        edges {
          node {
            body {
            ... on PRISMIC_PageBodyPreisliste {
              primary {
                price_section_title
              }
              fields {
                price
                price_annex
                price_description
                price_list_title
                price_type
              }
            }
          }
            page_title
            content
            _meta {
              uid
              id
            }
          }
        }
      }
    }
  }
`
export default function page(props) {
  let pageTitle = "";
  let content = "";
  let sliceZoneBody = null;
  if (props.data.prismic.allPages.edges[0]) {
    sliceZoneBody = props.data.prismic.allPages.edges[0].node.body;
    pageTitle = props.data.prismic.allPages.edges[0].node.page_title;
    content = props.data.prismic.allPages.edges[0].node.content;
  }

  let pricelist = null;

  /*   if (props.location.pathname === "/kontakt" || props.location.pathname === "/kontakt/") {
      contactSection = (
    <TwoColSection 
      colleft={<ContactForm data={props.data} />} 
      colright={<StyledAddress pageaddress><Address data={props.data} /></StyledAddress>} />
      )
    } */

  // Check if there is a picelist on the page
  if (sliceZoneBody) {
    pricelist = (
      <PriceList prices={sliceZoneBody[0].fields}
        title={sliceZoneBody[0].primary.price_section_title}
      />
    )
  }

  return (
    <Layout>
      <PageTitle pageTitle={pageTitle} />
      <OneColTextSection content={content} />
      <OneColSection content={pricelist} />
    </Layout>
  );
}